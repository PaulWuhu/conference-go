import requests
from events.keys import PEXELS_API_KEY, WEATHER_API_KEY


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search?"
    header = {"Authorization": PEXELS_API_KEY}
    params = {"query": city + " " + state, "per_page": 1}
    response = requests.get(url, headers=header, params=params)
    print("here is the print", response.content)
    data = response.json()
    picture_url = {"picture_url": data["photos"][0]["src"]["original"]}
    return picture_url


def get_weather_data(city, state):
    geourl = " http://api.openweathermap.org/geo/1.0/direct?"
    params = {
        "q": f"{city},{state},US",
        "appid": WEATHER_API_KEY,
        "limit": 1,
    }
    geo_response = requests.get(geourl, params=params)
    geo_data = geo_response.json()
    try:
        lat = geo_data[0]["lat"]
        lon = geo_data[0]["lon"]
    except (KeyError, IndexError):
        return None
    weather_url = "https://api.openweathermap.org/data/2.5/weather?"
    params = {
        "lat": lat,
        "lon": lon,
        "appid": WEATHER_API_KEY,
        "units": "imperial",
    }
    weather_response = requests.get(weather_url, params=params)
    weather_data = weather_response.json()
    try:
        description = weather_data["weather"][0]["description"]
        temp = weather_data["main"]["temp"]
    except (KeyError, IndexError):
        return None
    return {"description": description, "temp": temp}
