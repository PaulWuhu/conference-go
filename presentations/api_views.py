from django.http import JsonResponse
from events.models import Conference
from .models import Presentation, Status
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title"]

    def get_extra_data(self, object):
        return {"status": object.status.name}


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentation = Presentation.objects.filter(conference=conference_id)
        # presentations = [
        #     {
        #         "title": p.title,
        #         "status": p.status.name,
        #         "href": p.get_api_url(),
        #     }
        #     for p in Presentation.objects.filter(conference=conference_id)
        # ]
        # return {"presentations": presentations}
        return JsonResponse(presentation, PresentationListEncoder, safe=False)
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=content["conference"])
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            PresentationDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentations = Presentation.objects.get(id=id)
        # ans = {
        #     "presenter_name":presentations.presenter_name,
        #     "company_name":presentations.company_name,
        #     "presenter_email":presentations.presenter_email,
        #     "title":presentations.title,
        #     "synopsis":presentations.synopsis,
        #     "created": presentations.created,
        #     "status": presentations.status.name,
        #     "conference": {
        #         "name": presentations.conference.name,
        #         "href": presentations.conference.get_api_url(),
        #     }
        # }
        return JsonResponse(
            presentations, PresentationDetailEncoder, safe=False
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "conference" in content:
                conference = Conference.objects.get(id=content["conference"])
                content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        Presentation.objects.update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            PresentationDetailEncoder,
            safe=False,
        )
