from datetime import datetime
from json import JSONEncoder
from django.db.models import QuerySet

# this doesnt work with datetime
# class ModelEncoder(JSONEncoder):
#     def default(self, object):
#         d = {}
#         for property in self.properties:
#             d[property] = getattr(object, property)
#         return d


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(QuerySetEncoder, DateEncoder, JSONEncoder):
    encoders = {}

    def default(self, object):
        if isinstance(object, self.model):
            d = {}
            if hasattr(object, "get_api_url"):
                d["href"] = object.get_api_url()
            for property in self.properties:
                value = getattr(object, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d[property] = value
            d.update(self.get_extra_data(object))
            return d
        else:
            return super().default(object)

    def get_extra_data(self, object):
        return {}
